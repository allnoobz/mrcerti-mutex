# -*- coding: utf-8 -*-
import click

from mrcerti.mutex.daemon.server import (
    start_mutex_server,
    stop_mutex_server,
)

mutex_server = click.Group()
mutex_server.command(name='start')(start_mutex_server)
mutex_server.command(name='stop')(stop_mutex_server)


def main():
    """ entry point for setup.py """
    mutex_server()

if __name__ == '__main__':
    main()
