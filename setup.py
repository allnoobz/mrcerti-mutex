#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name='mrcerti-mutex',
    version='0.0',
    description='mrcerti-mutex',
    author='',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires="""
click==2.4
Twisted==14.0.0
""",
    entry_points={
        'console_scripts': [
            'mrcerti_mutex = mrcerti_mutex:main',
        ],
    }
)
