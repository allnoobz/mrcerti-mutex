from collections import deque
import json
import logging
import time

from twisted.internet import reactor
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver

DEFAULT_PORT = 8123

MUTEX_ACQUIRE = 'acquire'
MUTEX_RELEASE = 'release'
MUTEX_STATE = 'state'
PRINT_MUTEXES = 'mutexes'


class Mutex(object):

    def __init__(self, name, timeout):
        self.name = name
        self.timeout = timeout
        self.time = time.time()
        self.owner = None
        self.locked = False
        self.released_owners = set()
        self.queue = deque([])

    def force_release(self, acquired_time, owner):
        if self.time == acquired_time and self.owner == owner:
            logging.warn("forced releasing mutex '%s' - timeout", self.name)
            self.released_owners.add(self.owner)
            self.release(self.owner)

    def acquire(self, client_id, transport):
        if client_id in self.released_owners:
            logging.error("released client trying to acquire mutex")
            response = {
                "acquired": False,
            }
            transport.write(json.dumps(response))
            transport.loseConnection()
            return
        if not self.locked:
            logging.info("acquiring mutex '%s' by client %s"
                         % (self.name, client_id))
            self.locked = True
            logging.info("client %s has mutex" % client_id)
            self.owner = client_id
            self.time = time.time()
            reactor.callLater(
                self.timeout,
                self.force_release,
                self.time,
                self.owner
            )
            response = {
                "acquired": True,
            }
            transport.write(json.dumps(response))
            transport.loseConnection()
        else:
            self.queue.append((client_id, transport))
            logging.info(
                "client %s added to queue, len(queue) = %s"
                % (client_id, len(self.queue))
            )

    def release(self, client_id):
        assert (
            self.owner == client_id and self.locked
            or client_id in self.released_owners
        )
        if self.owner is not client_id and client_id in self.released_owners:
            logging.warn(
                "Trying to release mutex '%s' by released owner - 'client %s'"
                % (self.name, client_id)
            )
            return
        self.locked = False
        if len(self.queue) == 0:
            self.owner = None
        else:
            client_id, transport = self.queue.popleft()
            self.acquire(client_id, transport)

    def state(self):
        logging.info(
            "__MUTEX__\n"
            "name = %s\ntime = %s\nowner = %s\nlocked = %s\nqueue=%s"
            % (self.name, self.time, self.owner, self.locked, self.queue)
        )


class MutexManager(LineReceiver):

    def __init__(self, mutexes, mutex_timeout):
        self.mutexes = mutexes
        self.mutex_timeout = mutex_timeout

    @staticmethod
    def connectionMade():
        logging.info("----- NEW CONNECTION -----")

    def lineReceived(self, line):
        data = json.loads(line)
        if len(data) == 2:
            assert data['action'] == PRINT_MUTEXES
            self.print_mutexes()
        elif len(data) == 3:
            action = data['action']
            mutex_name = data['mutex']
            client_id = data['client_id']
            if action == MUTEX_ACQUIRE:
                self.mutex_acquire(mutex_name, client_id)
            elif action == MUTEX_RELEASE:
                self.mutex_release(mutex_name, client_id)
            elif action == MUTEX_STATE:
                self.mutex_state(mutex_name)
            else:
                assert False
        else:
            assert False

    @staticmethod
    def connectionLost(reason):
        logging.warn("connection lost --> %s", reason)

    def print_mutexes(self):
        for name in self.mutexes:
            self.mutexes[name].state()

    def mutex_state(self, name):
        if name in self.mutexes:
            self.mutexes[name].state()
        else:
            logging.warn("There is no %s mutex" % name)

    def mutex_acquire(self, name, client_id):
        if name not in self.mutexes:
            self.mutexes[name] = Mutex(name, self.mutex_timeout)
        self.mutexes[name].acquire(client_id, self.transport)

    def mutex_release(self, name, client_id):
        if name in self.mutexes:
            logging.info("release mutex '%s' by client %s" % (name, client_id))
            self.mutexes[name].release(client_id)
        else:
            logging.warn("There is no %s mutex" % name)


class MutexManagerFactory(Factory):

    def __init__(self, mutex_timeout=60):
        self.mutexes = {}  # maps mutexes to MutexManager instances
        self.mutex_timeout = mutex_timeout

    def buildProtocol(self, _):
        return MutexManager(self.mutexes, self.mutex_timeout)


def clean_reactor():
    calls = reactor.getDelayedCalls()
    for call in calls:
        call.cancel()

if __name__ == '__main__':
    try:
        reactor.listenTCP(DEFAULT_PORT, MutexManagerFactory())
        reactor.run(installSignalHandlers=True)
    except Exception as exc:
        logging.exception(exc)

