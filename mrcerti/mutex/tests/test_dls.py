import threading

from twisted.internet import reactor

from mrcerti.mutex.client import Client
from mrcerti.mutex.server import MutexManagerFactory, clean_reactor
from mrcerti.mutex.testing import UnitTestCase

SERVER_PORT = 55555
MUTEX_TIMEOUT = 2


class ServerThread(threading.Thread):

    def __init__(self, tcp_port=SERVER_PORT):
        threading.Thread.__init__(self)
        self.port_number = tcp_port
        self.port_object = None

    def run(self):
        self.port_object = reactor.listenTCP(
            self.port_number,
            MutexManagerFactory(MUTEX_TIMEOUT)
        )
        reactor.run(installSignalHandlers=False)

    def stop(self):
        self.port_object.stopListening()
        clean_reactor()


class DlsTestCase(UnitTestCase):

    def _start_server(self):
        server = ServerThread()
        server.setDaemon(True)
        server.start()
        return server

    def _new_client(self):
        return Client(tcp_port=SERVER_PORT)

    def test_dls(self):
        server = self._start_server()
        client1 = self._new_client()
        client1.acquire()
        self.assertEqual(client1.mutex_amount, 1)
        client1.acquire()
        self.assertEqual(client1.mutex_amount, 2)
        client2 = self._new_client()
        client2.acquire()
        self.assertEqual(client2.mutex_amount, 1)
        client3 = self._new_client()
        client3.acquire()
        self.assertEqual(client3.mutex_amount, 1)
        client2.release()
        self.assertFalse(client2.acquire())
        server.stop()

