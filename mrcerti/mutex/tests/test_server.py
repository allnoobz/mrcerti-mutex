import json

from twisted.test import proto_helpers

from mrcerti.mutex.server import MutexManagerFactory, clean_reactor
from mrcerti.mutex.testing import UnitTestCase


class ServerTestCase(UnitTestCase):

    def setUp(self):
        factory = MutexManagerFactory()
        self.protocol = factory.buildProtocol(('127.0.0.1', 0))
        self.transport = proto_helpers.StringTransport()
        self.protocol.makeConnection(self.transport)

    def _test(self, client_id, action, mutex_name="test"):
        data = {
            "client_id": client_id,
            "action": action,
            "mutex": mutex_name,
        }
        self.protocol.dataReceived(json.dumps(data) + "\r\n")

    def _clean_server(self):
        clean_reactor()

    def _get_mutex_clients_queue(self, mutex_name="test"):
        queue = self._get_mutex(mutex_name).queue
        return [int(pair[0]) for pair in queue]

    def _get_mutex(self, mutex_name="test"):
        return self.protocol.mutexes.get(mutex_name)

    def test_create_mutex(self):
        self.assertEqual(
            len(self.protocol.mutexes), 0,
            "Mutexes should be empty"
        )
        self._test(1, "acquire")
        self.assertEqual(
            len(self.protocol.mutexes), 1,
            "Mutexes should have exactly 1 mutex"
        )
        self.assertEqual(
            self.protocol.mutexes.keys(), ['test'],
            "Mutex name should be 'test'"
        )
        self._clean_server()

    def test_acquire_and_release(self):
        self.assertEqual(
            len(self.protocol.mutexes), 0,
            "Mutexes should be empty"
        )
        self._test(1, "acquire")
        self.assertEqual(
            len(self.protocol.mutexes), 1,
            "Mutexes should have exactly 1 mutex"
        )
        self.assertEqual(
            self.protocol.mutexes.keys(), ['test'],
            "Mutex name should be 'test'"
        )
        self.assertTrue(
            self._get_mutex().locked,
            "Test mutex should be locked"
        )
        self._test(1, "release")
        self.assertEqual(
            len(self.protocol.mutexes), 1,
            "Mutexes should have exactly 1 mutex"
        )
        self.assertFalse(
            self._get_mutex().locked,
            "Test mutex shouldn`t be locked"
        )
        self._clean_server()

    def test_multiple_mutex_acquire(self):
        self.assertEqual(
            len(self.protocol.mutexes), 0,
            "Mutexes should be empty"
        )
        self._test(1, "acquire", "test1")
        self.assertEqual(
            len(self.protocol.mutexes), 1,
            "Mutexes should have exactly 1 mutex"
        )
        self._test(1, "acquire", "test2")
        self.assertEqual(
            len(self.protocol.mutexes), 2,
            "Mutexes should have exactly 2 mutexes"
        )
        self._test(1, "acquire", "test3")
        self.assertEqual(
            len(self.protocol.mutexes), 3,
            "Mutexes should have exactly 3 mutexes"
        )
        self._clean_server()

    def test_mutex_queue(self):
        self.assertEqual(
            len(self.protocol.mutexes), 0,
            "Mutexes should be empty"
        )
        self._test(1, "acquire")
        self.assertEqual(
            len(self.protocol.mutexes), 1,
            "Mutexes should have exactly 1 mutex"
        )
        self.assertEqual(
            self.protocol.mutexes.keys(), ['test'],
            "Mutex name should be 'test'"
        )
        self.assertEqual(
            len(self._get_mutex().queue), 0,
            "'test' mutex queue should be empty"
        )
        self._test(2, "acquire")
        self.assertEqual(
            self._get_mutex_clients_queue(), [2],
            "Client 2 should be in 'test' mutex queue"
        )
        self._test(3, "acquire")
        self.assertEqual(
            self._get_mutex_clients_queue(), [2, 3],
            "Clients 2 and 3 should be in 'test' mutex queue"
        )
        self.assertEqual(
            self._get_mutex().owner, 1,
            "Client 1 should be 'test' mutex owner"
        )
        self._test(1, "release")
        self.assertEqual(
            self._get_mutex().owner, 2,
            "Client 2 should be 'test' mutex owner"
        )
        self._test(4, "acquire")
        self.assertEqual(
            self._get_mutex_clients_queue(), [3, 4],
            "Client 3 and 4 should be in 'test' mutex queue"
        )
        self._test(2, "release")
        self.assertEqual(
            self._get_mutex().owner, 3,
            "Client 3 should be 'test' mutex owner"
        )
        self.assertEqual(
            self._get_mutex_clients_queue(), [4],
            "Client 4 should be in 'test' mutex queue"
        )
        self._test(3, "release")
        self.assertEqual(
            self._get_mutex().owner, 4,
            "Client 4 should be 'test' mutex owner"
        )
        self.assertEqual(
            len(self._get_mutex().queue), 0,
            "'test' mutex queue should be empty"
        )
        self._clean_server()
