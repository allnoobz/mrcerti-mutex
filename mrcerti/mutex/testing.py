from twisted.trial import unittest
import logging.config
import os


def get_test_config_file():
    return os.path.abspath('base.ini')


class UnitTestCase(unittest.TestCase):

    def setUp(self):
        logging.config.fileConfig(get_test_config_file())