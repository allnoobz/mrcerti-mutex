# -*- coding: utf-8 -*-

import json
import logging
import socket
import uuid
from zope.interface.declarations import implements
from zope.interface.interface import Interface
from mrcerti.mutex.server import DEFAULT_PORT


class IMutexClient(Interface):
    pass


class MutexClient(object):
    implements(IMutexClient)

    def __init__(self, mutex_name="default", tcp_ip="127.0.0.1",
                 tcp_port=DEFAULT_PORT):
        self.mutex_name = mutex_name
        self.mutex_amount = 0
        self.tcp_ip = tcp_ip
        self.tcp_port = tcp_port
        self.id = str(uuid.uuid1())
        self.sock = None

    def acquire(self):
        acquired = True
        if self.mutex_amount == 0:
            self.connect()
            self.get_mutex()
            response = json.loads(self.read_line())
            logging.info(
                "'%s'.acquire() response : %s"
                % (self.mutex_name, response)
            )
            acquired = response['acquired']
            self.disconnect()
        if acquired:
            self.mutex_amount += 1
        return acquired

    def connect(self):
        logging.info("client %s connecting", self.id)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(600)
        self.sock.connect((self.tcp_ip, self.tcp_port))

    def disconnect(self):
        try:
            logging.info("client %s disconnecting", self.id)
            self.sock.shutdown(socket.SHUT_RDWR)
            self.sock.close()
        except socket.error:
            logging.info("client %s already disconnected")

    def get_mutex(self):
        data = {
            "client_id": self.id,
            "action": "acquire",
            "mutex": self.mutex_name,
        }
        status = self.sock.sendall(json.dumps(data) + "\r\n")
        assert status is None

    def read_line(self):
        return self.sock.makefile().readline()

    def release(self):
        self.mutex_amount -= 1
        if self.mutex_amount == 0:
            self.connect()
            self.release_mutex()
            self.disconnect()

    def release_mutex(self):
        data = {
            "client_id": self.id,
            "action": "release",
            "mutex": self.mutex_name,
        }
        status = self.sock.sendall(json.dumps(data) + "\r\n")
        assert status is None