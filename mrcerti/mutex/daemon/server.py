from twisted.internet import reactor

from mrcerti.mutex.daemon.base import Daemon
from mrcerti.mutex.server import (
    DEFAULT_PORT,
    MutexManagerFactory,
)

PID_FILE = '/tmp/mrcerti-mutex-server-daemon.pid'
STDOUT_LOG = '/tmp/mrcerti-mutex-server-daemon.stdout'
STDERR_LOG = '/tmp/mrcerti-mutex-server-daemon.stderr'


class MutexServerDaemon(Daemon):

    def __init__(self, tcp_port=DEFAULT_PORT, *args, **kwargs):
        Daemon.__init__(self, *args, **kwargs)
        self.tcp_port = tcp_port

    def run(self):
        reactor.listenTCP(self.tcp_port, MutexManagerFactory())
        reactor.run(installSignalHandlers=False)


def start_mutex_server(tcp_port=DEFAULT_PORT, pid_file=PID_FILE):
    daemon = MutexServerDaemon(
        tcp_port=tcp_port,
        pidfile=pid_file,
        stdout=STDOUT_LOG,
        stderr=STDERR_LOG,
    )
    daemon.start()


def stop_mutex_server(pid_file=PID_FILE):
    daemon = MutexServerDaemon(
        tcp_port=None,
        pidfile=pid_file,
    )
    daemon.stop()